/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scp_problem.hpp"
#include "scp_grasp.hpp"
#include "scp_local_search.hpp"
#include "scp_descent.hpp"
#include "chrono.hpp"
#include "gzfstream.hpp"
#include "docopt.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <limits>
#include <stdexcept>
#include <cstdlib>
#include <ctime>

enum
{
	format_scp,
	format_rail,
	format_stcp,
	format_aa
};

void run_grasp( const scp_problem & instance, double alpha, double epsilon, int num_runs = 1 )
{
	scp_grasp grasp;
	scp_local_search local_search;
	chrono::time_point begin, end;
	double avg_z = 0,
	       min_z = std::numeric_limits<double>::infinity(),
	       max_z = -std::numeric_limits<double>::infinity(),
	       num_1_0 = 0, num_1_1 = 0, num_2_1 = 0;

	// Initialize parameters

	grasp.alpha = alpha;
	grasp.epsilon = epsilon;

	// Load the instance in algorithms

	grasp.read( instance );
	local_search.read( instance );

	begin = chrono::now();
	for ( int i = 0; i < num_runs; ++i )
	{
		if ( !grasp.run() )
		{
			throw std::runtime_error( "GRASP fail" );
		}

#if 0
		if ( !instance.check( grasp.x(), grasp.z() ) )
		{
			throw std::runtime_error( "GRASP check fail" );
		}
#endif

		local_search.read( grasp.x() );

		if ( !local_search.run() )
		{
			throw std::runtime_error( "Local search fail" );
		}

#if 0
		if ( !instance.check( local_search.x(), local_search.z() ) )
		{
			throw std::runtime_error( "Local search check fail" );
		}
#endif

		if ( local_search.z() < min_z )
		{
			min_z = local_search.z();
		}

		if ( local_search.z() > max_z )
		{
			max_z = local_search.z();
		}

		avg_z = ( double( i ) * avg_z + double( local_search.z() ) ) / double( i+1 );
		num_1_0 = ( double( i ) * num_1_0 + double( local_search.num_1_0() ) ) / double( i+1 );
		num_1_1 = ( double( i ) * num_1_1 + double( local_search.num_1_1() ) ) / double( i+1 );
		num_2_1 = ( double( i ) * num_2_1 + double( local_search.num_2_1() ) ) / double( i+1 );
	}
	end = chrono::now();

	std::cout << "GRASP+LS z (min/avg/max) = " << min_z << " / " << avg_z << " / " << max_z << std::endl;
	std::cout << "GRASP+LS time = " << ( end - begin ) / double( num_runs ) << std::endl;
	std::cout << "GRASP+LS 1-0 opt = " << num_1_0 << std::endl;
	std::cout << "GRASP+LS 1-1 opt = " << num_1_1 << std::endl;
	std::cout << "GRASP+LS 2-1 opt = " << num_2_1 << std::endl;
}

void run_descent( const scp_problem & instance, double epsilon, int num_runs = 1 )
{
	scp_descent descent;
	chrono::time_point begin, end;

	// Initialize parameters

	descent.epsilon = epsilon;

	// Load the instance in algorithms

	descent.read( instance );

	begin = chrono::now();
	for ( int i = 0; i < num_runs; ++i )
	{
		if ( !descent.run() )
		{
			throw std::runtime_error( "Descent Heuristic fail" );
		}

#if 0
		if ( !instance.check( grasp.x(), grasp.z() ) )
		{
			throw std::runtime_error( "GRASP check fail" );
		}
#endif
	}
	end = chrono::now();

	std::cout << "DH z = " << descent.z() << std::endl;
	std::cout << "DH time = " << ( end - begin ) / double( num_runs ) << std::endl;
}

int main( int argc, char * argv[] )
{
	docopt::option_group options;
	docopt::parser opt_parser;

	int num_runs = 1, format = format_scp;
	double alpha = 0.8, epsilon = 1e-9;
	unsigned int seed = std::time( 0 );
	bool verbose = true, help = false;
	std::string format_str, filename;

	scp_problem instance;

	// Initialize option parser

	options.name( "Options:" )
		( "alpha",   'a', "FLOAT",  "RCL threshold parameter (in [0,1])" )
		(            'n', "INT",    "Number of tries" )
		( "random",  'r', "INT",    "Random seed" )
		( "epsilon", 'e', "FLOAT",  "Tolerance" )
		( "format",  'f', "FORMAT", "Instance file format (scp, rail, stcp, aa)" )
		( "quiet",   'q',           "Don't produce any verbose output" )
		( docopt::option::help() );

	opt_parser
		.add( options )
		.bind( 'a', &alpha )
		.bind( 'n', &num_runs )
		.bind( 'r', &seed )
		.bind( 'e', &epsilon )
		.bind( 'f', &format_str )
		.bind<bool, false>( 'q', &verbose )
		.bind( '?', &help )
		.bind_arguments( &filename );

	opt_parser.parse( argc, argv );

	// Print help

	if ( help || filename.empty() )
	{
		std::cout
			<< "Usage: " << argv[0] << " [OPTIONS] FILE" << std::endl
			<< std::endl
			<< options << std::endl;
		return 0;
	}

	// Select format

	if ( format_str == "rail" )
	{
		format = format_rail;
	}
	else if ( format_str == "stcp" )
	{
		format = format_stcp;
	}
	else if ( format_str == "aa" )
	{
		format = format_aa;
	}

	// Print options

	if ( verbose )
	{
		switch ( format )
		{
			case format_rail:
				std::cout << "format  = rail" << std::endl;
				break;
			case format_stcp:
				std::cout << "format  = stcp" << std::endl;
				break;
			case format_aa:
				std::cout << "format  = aa" << std::endl;
				break;
			case format_scp:
			default:
				std::cout << "format  = scp" << std::endl;
				break;
		};
		std::cout
			<< "alpha   = " << alpha << std::endl
			<< "n       = " << num_runs << std::endl
			<< "random  = " << seed << std::endl
			<< "epsilon = " << epsilon << std::endl;
	}

	// Initialize random number generator

	std::srand( seed );

	// Open and load the instance file

	if ( filename.substr( filename.find_last_of( "." ) + 1 ) == "gz" )
	{
		// Read gzip compressed instance
		gz::ifstream file( filename.c_str() );
		if ( !file.is_open() )
		{
			std::cerr << "Error opening '" << filename << "'" << std::endl;
			return 0;
		}
		instance.read( file );
		file.close();
	}
	else if ( filename != "-" )
	{
		// Read uncompressed instance from file
		std::ifstream file( filename.c_str() );
		if ( !file.is_open() )
		{
			std::cerr << "Error opening '" << filename << "'" << std::endl;
			return 0;
		}
		instance.read( file );
		file.close();
	}
	else
	{
		// Read uncompressed instance from terminal
		instance.read( std::cin );
	}

	if ( verbose )
	{
		std::cout << "Size: " << instance.num_rows() << " x " << instance.num_cols() << std::endl
		          << "Density: " << 100. * instance.density() << " %" << std::endl;
	}

	run_grasp( instance, alpha, epsilon, num_runs );
	run_descent( instance, epsilon, num_runs );

	return 0;
}
