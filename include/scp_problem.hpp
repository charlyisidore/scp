/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCP_PROBLEM_HPP
#define SCP_PROBLEM_HPP

#include <iostream>
#include <vector>
#include <set>

/*
	Class: scp_problem

	Set Cover Problem instance.
*/
struct scp_problem
{
	int num_rows() const;
	int num_cols() const;
	double density() const;

	void clear();
	void reduce();

	double z( const std::set<int> & x ) const;

	bool check() const;
	bool check( const std::set<int> & x ) const;
	bool check( const std::set<int> & x, double obj, double epsilon = 1e-9 ) const;

	void read( std::istream & is );
	void read_rail( std::istream & is );
	void read_stcp( std::istream & is );
	void read_aa( std::istream & is );

	void write( std::ostream & os ) const;

	std::vector<double> c;
	std::vector< std::set<int> > S;
};

////////////////////////////////////////////////////////////////////////////////

inline int scp_problem::num_rows() const
{
	return S.size();
}

inline int scp_problem::num_cols() const
{
	return c.size();
}

inline double scp_problem::density() const
{
	int m = num_rows(), n = num_cols();
	int p = 0;

	for ( int i = 0; i < m; ++i )
	{
		p += S[i].size();
	}
	return double( p ) / double( m * n );
}

inline void scp_problem::clear()
{
	c.clear();
	S.clear();
}

inline void scp_problem::reduce()
{
	int m = num_rows(), n = num_cols();
	std::set<int>::iterator it;

	for ( int j = 0; j < n; ++j )
	{
		bool found = false;

		for ( int i = 0; !found && i < m; ++i )
		{
			if ( S[i].find( j ) != S[i].end() )
			{
				found = true;
			}
		}

		if ( !found )
		{
			for ( int i = 0; !found && i < m; ++i )
			{
				it = S[i].lower_bound( j );

				if ( it != S[i].end() )
				{
					if ( *it == j )
						S[i].erase( it++ );
					else
						++it;

					while ( it != S[i].end() )
					{
						int k = *it;
						S[i].erase( it++ );
						S[i].insert( k-1 );
					}
				}
			}
		}
	}
}

inline double scp_problem::z( const std::set<int> & x ) const
{
	std::set<int>::const_iterator j;
	double obj = 0;

	for ( j = x.begin(); j != x.end(); ++j )
	{
		obj += c[*j];
	}
	return obj;
}

inline bool scp_problem::check() const
{
	int m = num_rows();

	for ( int i = 0; i < m; ++i )
	{
		if ( S[i].empty() )
		{
			return false;
		}
	}
	return true;
}

inline bool scp_problem::check( const std::set<int> & x ) const
{
	int m = num_rows();
	std::set<int>::const_iterator j;

	for ( int i = 0; i < m; ++i )
	{
		bool covered = false;

		for ( j = S[i].begin(); !covered && j != S[i].end(); ++j )
		{
			if ( x.find( *j ) != x.end() )
			{
				covered = true;
			}
		}

		if ( !covered )
		{
			return false;
		}
	}
	return true;
}

inline bool scp_problem::check( const std::set<int> & x, double obj, double epsilon ) const
{
	double diff = z( x ) - obj;
	return -epsilon <= diff && diff <= epsilon && check( x );
}

inline void scp_problem::read( std::istream & is )
{
	int m, n, p, l;

	is >> m >> n;

	clear();
	c.resize( n );
	S.resize( m );

	for ( int j = 0; j < n; ++j )
	{
		is >> c[j];
	}

	for ( int i = 0; i < m; ++i )
	{
		is >> p;

		for ( int k = 0; k < p; ++k )
		{
			is >> l;
			S[i].insert( l-1 );
		}
	}
}

inline void scp_problem::read_rail( std::istream & is )
{
	int m, n, p, l;

	is >> m >> n;

	clear();
	c.resize( n );
	S.resize( m );

	for ( int j = 0; j < n; ++j )
	{
		is >> c[j] >> p;

		for ( int k = 0; k < p; ++k )
		{
			is >> l;
			S[l-1].insert( j );
		}
	}
}

inline void scp_problem::read_stcp( std::istream & is )
{
	int m, n, l;

	is >> n >> m;

	clear();
	c.resize( n, 1 );
	S.resize( m );

	for ( int i = 0; i < m; ++i )
	{
		for ( int k = 0; k < 3; ++k )
		{
			is >> l;
			S[i].insert( l-1 );
		}
	}
}

inline void scp_problem::read_aa( std::istream & is )
{
	int m, n, p, l;

	is >> n >> m;

	clear();
	c.resize( n );
	S.resize( m );

	for ( int j = 0; j < n; ++j )
	{
		is >> c[j];
	}

	for ( int j = 0; j < n; ++j )
	{
		is >> p;

		for ( int k = 0; k < p; ++k )
		{
			is >> l;
			S[l-1].insert( j );
		}
	}
}

inline void scp_problem::write( std::ostream & os ) const
{
	int m = num_rows(),
	    n = num_cols();

	os << m << ' ' << n << std::endl;

	for ( int j = 0; j < n; ++j )
	{
		if ( j > 0 ) os << ' ';
		os << c[j];
	}
	os << std::endl;

	for ( int i = 0; i < m; ++i )
	{
		std::set<int>::const_iterator j;

		os << S[i].size() << std::endl;
		for ( j = S[i].begin(); j != S[i].end(); ++j )
		{
			os << ' ' << *j+1;
		}
		os << std::endl;
	}
}

inline std::istream & operator >> ( std::istream & is, scp_problem & p )
{
	p.read( is );
	return is;
}

inline std::ostream & operator << ( std::ostream & os, const scp_problem & p )
{
	p.write( os );
	return os;
}

#endif
