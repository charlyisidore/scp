/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCP_LOCAL_SEARCH_HPP
#define SCP_LOCAL_SEARCH_HPP

#include <vector>
#include <set>
#include <algorithm>
#include "scp_problem.hpp"

/*
	Class: scp_local_search

	Set Cover Problem solver using GRASP metaheuristic.
*/
struct scp_local_search
{
	enum
	{
		_1_0 = (1 << 0),
		_1_1 = (1 << 1),
		_2_1 = (1 << 2)
	};

	scp_local_search( double eps = 1e-9 );

	void read( const scp_problem & instance );
	void read( const std::vector<double> & c );
	void read( const std::set<int> & x );
	bool run( int exchanges = (_1_0 | _1_1 | _2_1) );

	double z() const;
	std::set<int> x() const;

	int num_exchanges() const;
	int num_1_0() const;
	int num_1_1() const;
	int num_2_1() const;

	double epsilon;

private:
	std::vector<double> _c;
	std::vector< std::vector<int> > _S, _T;
	std::set<int> _x;
	double _z;
	int _num_1_0,
	    _num_1_1,
	    _num_2_1;

	void run_1_0( std::vector<int> & v );
	void run_1_1( std::vector<int> & v );
	void run_2_1( std::vector<int> & v );
};

////////////////////////////////////////////////////////////////////////////////

inline scp_local_search::scp_local_search( double eps ) :
	epsilon( eps ),
	_z( 0 ),
	_num_1_0( 0 ),
	_num_1_1( 0 ),
	_num_2_1( 0 )
{
}

inline void scp_local_search::read( const scp_problem & instance )
{
	int m = instance.num_rows(),
	    n = instance.num_cols();

	std::set<int>::const_iterator j;

	_c.clear();
	_S.clear();
	_T.clear();

	_c = instance.c;
	_S.resize( m );
	_T.resize( n );

	for ( int i = 0; i < m; ++i )
	{
		for ( j = instance.S[i].begin(); j != instance.S[i].end(); ++j )
		{
			_S[i].push_back( *j );
			_T[*j].push_back( i );
		}
	}
}

inline void scp_local_search::read( const std::vector<double> & c )
{
	_c = c;
}

inline void scp_local_search::read( const std::set<int> & x )
{
	_x = x;
}

inline bool scp_local_search::run( int exchanges )
{
	int m = _S.size();
	std::vector<int>::iterator i;
	std::set<int>::iterator j;
	std::vector<int> v( m, 0 );

	_z = 0;

	for ( j = _x.begin(); j != _x.end(); ++j )
	{
		_z += _c[*j];

		for ( i = _T[*j].begin(); i != _T[*j].end(); ++i )
		{
			++v[*i];
		}
	}

	_num_1_0 = 0;
	_num_1_1 = 0;
	_num_2_1 = 0;

	if ( exchanges & _1_0 ) run_1_0( v );
	if ( exchanges & _1_1 ) run_1_1( v );
	if ( exchanges & _2_1 ) run_2_1( v );
	return true;
}

inline void scp_local_search::run_1_0( std::vector<int> & v )
{
	std::vector<int>::iterator i;
	std::set<int>::iterator j;

	// 1-0 exchange
	for ( j = _x.begin(); j != _x.end(); )
	{
		bool is_useless = ( _c[*j] + epsilon >= 0 );

		for ( i = _T[*j].begin(); is_useless && i != _T[*j].end(); ++i )
		{
			if ( v[*i] <= 1 )
			{
				is_useless = false;
			}
		}

		if ( is_useless )
		{
			for ( i = _T[*j].begin(); is_useless && i != _T[*j].end(); ++i )
			{
				--v[*i];
			}

			_z -= _c[*j];
			_x.erase( j++ );
			++_num_1_0;
		}
		else
		{
			++j;
		}
	}
}

inline void scp_local_search::run_1_1( std::vector<int> & v )
{
	int n = _T.size();

	std::vector<int>::iterator i;
	std::set<int>::iterator j;
	bool improved = true;

	// 2-1 exchange
	while ( improved )
	{
		improved = false;
		for ( j = _x.begin(); !improved && j != _x.end(); ++j )
		{
			std::set<int> to_cover;

			for ( i = _T[*j].begin(); i != _T[*j].end(); ++i )
			{
				if ( v[*i] <= 1 )
				{
					to_cover.insert( *i );
				}
			}

			for ( int k = 0; !improved && k < n; ++k )
			{
				if ( _c[k] - _c[*j] < 0             // Profitable exchange?
				     && _x.find( k ) == _x.end()    // Is the item not already selected?
				     && std::includes( _T[k].begin(), _T[k].end(), to_cover.begin(), to_cover.end() ) ) // Covers the subsets that need coverage?
				{
					for ( i = _T[*j].begin(); i != _T[*j].end(); ++i )
					{
						--v[*i];
					}

					for ( i = _T[k].begin(); i != _T[k].end(); ++i )
					{
						++v[*i];
					}

					_z += _c[k] - _c[*j];
					_x.erase( j );
					_x.insert( k );

					improved = true;
					++_num_1_1;
				}
			}
		}
	}
}

inline void scp_local_search::run_2_1( std::vector<int> & v )
{
	int m = _S.size(),
	    n = _T.size();

	std::vector<int>::iterator i;
	std::set<int>::iterator j, k;
	bool improved = true;

	// 2-1 exchange
	while ( improved )
	{
		improved = false;
		for ( j = _x.begin(); !improved && j != _x.end(); ++j )
		{
			k = j;

			for ( ++k; !improved && k != _x.end(); ++k )
			{
				std::vector<int> v_jk = v;
				std::set<int> to_cover;

				for ( i = _T[*j].begin(); i != _T[*j].end(); ++i )
				{
					--v_jk[*i];
				}

				for ( i = _T[*k].begin(); i != _T[*k].end(); ++i )
				{
					--v_jk[*i];
				}

				{
					for ( int i = 0; i < m; ++i )
					{
						if ( v_jk[i] <= 0 )
						{
							to_cover.insert( i );
						}
					}
				}

				for ( int l = 0; !improved && l < n; ++l )
				{
					if ( _c[l] - _c[*j] - _c[*k] < 0    // Profitable exchange?
					     && _x.find( l ) == _x.end()      // Is the item not already selected?
					     && std::includes( _T[l].begin(), _T[l].end(), to_cover.begin(), to_cover.end() ) ) // Covers the subsets that need coverage?
					{
						// v_jk is the new v vector
						for ( i = _T[l].begin(); i != _T[l].end(); ++i )
						{
							++v_jk[*i];
						}

						_z += _c[l] - _c[*j] - _c[*k];
						_x.erase( j );
						_x.erase( k );
						_x.insert( l );

						v = v_jk;
						improved = true;
						++_num_2_1;
					}
				}
			}
		}
	}
}

inline double scp_local_search::z() const
{
	return _z;
}

inline std::set<int> scp_local_search::x() const
{
	return _x;
}

inline int scp_local_search::num_exchanges() const
{
	return _num_1_0 + _num_1_1 + _num_2_1;
}

inline int scp_local_search::num_1_0() const
{
	return _num_1_0;
}

inline int scp_local_search::num_1_1() const
{
	return _num_1_1;
}

inline int scp_local_search::num_2_1() const
{
	return _num_2_1;
}

#endif
