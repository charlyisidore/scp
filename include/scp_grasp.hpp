/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCP_GRASP_HPP
#define SCP_GRASP_HPP

#include <vector>
#include <set>
#include <limits>
#include <cstdlib>
#include "scp_problem.hpp"

/*
	Class: scp_grasp

	Set Cover Problem solver using GRASP metaheuristic.
*/
struct scp_grasp
{
	scp_grasp( double a = 0.8, double eps = 1e-9 );

	void read( const scp_problem & instance );
	void read( const std::vector<double> & c );

	bool run();

	double z() const;
	std::set<int> x() const;

	double alpha,
	       epsilon;

private:
	std::vector<double> _c;
	std::vector< std::vector<int> > _S, _T;
	std::set<int> _x;
	double _z;

	int _add( int j, std::vector<int> & u, std::vector<int> & v );
};

////////////////////////////////////////////////////////////////////////////////

inline scp_grasp::scp_grasp( double a, double eps ) :
	alpha( a ), epsilon( eps ), _z( 0 )
{
}

inline void scp_grasp::read( const scp_problem & instance )
{
	int m = instance.num_rows(),
	    n = instance.num_cols();

	std::set<int>::const_iterator j;

	_c.clear();
	_S.clear();
	_T.clear();

	_c = instance.c;
	_S.resize( m );
	_T.resize( n );

	for ( int i = 0; i < m; ++i )
	{
		for ( j = instance.S[i].begin(); j != instance.S[i].end(); ++j )
		{
			_S[i].push_back( *j );
			_T[*j].push_back( i );
		}
	}
}

inline void scp_grasp::read( const std::vector<double> & c )
{
	_c = c;
}

inline bool scp_grasp::run()
{
	int m = _S.size(),
	    n = _T.size();

	std::vector<int> u( n, 0 ), v( m, 0 );
	std::vector<double> e( n, 0 );
	int num_covered = 0;

	_z = 0;
	_x.clear();

	for ( int j = 0; j < n; ++j )
	{
		u[j] = _T[j].size();
	}

	// Add all elements of negative or null cost
	for ( int j = 0; j < n; ++j )
	{
		if ( _c[j] <= epsilon )
		{
			num_covered += _add( j, u, v );
		}
	}

	while ( num_covered < m )
	{
		std::vector<int> rcl;
		double e_min = std::numeric_limits<double>::infinity(),
		       e_max = -std::numeric_limits<double>::infinity(),
		       e_limit = 0;
		int k;

		rcl.reserve( n );

		for ( int j = 0; j < n; ++j )
		{
			if ( u[j] > 0 )
			{
				e[j] = double( u[j] ) / _c[j];

				if ( e[j] < e_min ) e_min = e[j];
				if ( e[j] > e_max ) e_max = e[j];
			}
		}

		e_limit = e_min + alpha * ( e_max - e_min );

		for ( int j = 0; j < n; ++j )
		{
			if ( u[j] > 0 && e[j] + epsilon >= e_limit )
			{
				rcl.push_back( j );
			}
		}

		// Infeasible problem?
		if ( rcl.empty() ) return false;

		k = rcl[std::rand() % rcl.size()];
		num_covered += _add( k, u, v );
	}
	return true;
}

inline double scp_grasp::z() const
{
	return _z;
}

inline std::set<int> scp_grasp::x() const
{
	return _x;
}

inline int scp_grasp::_add( int j, std::vector<int> & u, std::vector<int> & v )
{
	std::vector<int>::const_iterator i, k;
	int n = 0;

	_x.insert( j );
	_z += _c[j];

	for ( i = _T[j].begin(); i != _T[j].end(); ++i )
	{
		if ( v[*i] == 0 )
		{
			++n;

			for ( k = _S[*i].begin(); k != _S[*i].end(); ++k )
			{
				--u[*k];
			}
		}
		++v[*i];
	}
	return n;
}

#endif
