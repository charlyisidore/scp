/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCP_DESCENT_HPP
#define SCP_DESCENT_HPP

#include <vector>
#include <set>
#include <limits>
#include <algorithm>
#include <cstdlib>
#include "scp_problem.hpp"

/*
	Class: scp_descent

	Set Cover Problem solver using descent heuristic.

	References:

		XU, Qingyuan, TAN, Anhui, et LIN, Yaojin. A rough set method for the
		unicost set covering problem. International Journal of Machine
		Learning and Cybernetics, p. 1-12.
		http://dx.doi.org/10.1155/2013/203032
*/
struct scp_descent
{
	scp_descent( double eps = 1e-9 );

	void read( const scp_problem & instance );
	void read( const std::vector<double> & c );
	bool run();

	double z() const;
	std::set<int> x() const;

	int num_iterations() const;

	double epsilon;

private:
	std::vector<double> _c, _g;
	std::vector< std::vector<int> > _S, _T;
	std::set<int> _x;
	double _z;
	int _num_iterations;

	void _flip( int j, std::vector<bool> & x, std::vector<int> & v );
};

////////////////////////////////////////////////////////////////////////////////

inline scp_descent::scp_descent( double eps ) :
	epsilon( eps ), _z( 0 ), _num_iterations( 0 )
{
}

inline void scp_descent::read( const scp_problem & instance )
{
	int m = instance.num_rows(),
	    n = instance.num_cols();

	std::set<int>::const_iterator j;

	_S.clear();
	_T.clear();

	_S.resize( m );
	_T.resize( n );

	for ( int i = 0; i < m; ++i )
	{
		for ( j = instance.S[i].begin(); j != instance.S[i].end(); ++j )
		{
			_S[i].push_back( *j );
			_T[*j].push_back( i );
		}
	}

	read( instance.c );
}

inline void scp_descent::read( const std::vector<double> & c )
{
	int m = _S.size(),
	    n = _T.size();

	_c.clear();
	_c = c;

	_g.clear();
	_g.resize( n, std::numeric_limits<double>::infinity() );

	// g(i) = c_min(e_i) + E
	for ( int i = 0; i < m; ++i )
	{
		std::vector<int>::const_iterator j;
		double c_max = -std::numeric_limits<double>::infinity();

		for ( j = _S[i].begin(); j != _S[i].end(); ++j )
		{
			if ( _c[*j] < _g[i] )
			{
				_g[i] = _c[*j];
			}
			if ( c_max < _c[*j] )
			{
				c_max = _g[i];
			}
		}

		_g[i] += 1.;
	}
}

inline bool scp_descent::run()
{
	int m = _S.size(),
	    n = _T.size();

	std::vector<int> v( m, 0 );
	std::vector<bool> x( n, false );
	double R_max = 1. + epsilon;

	_z = 0;
	_x.clear();
	_num_iterations = 0;

	for ( int j = 0; j < n; ++j )
	{
		if ( _c[j] <= epsilon )
		{
			_flip( j, x, v );
		}
	}

	while ( R_max > epsilon )
	{
		int j_max = -1;
		R_max = 0;

		for ( int j = 0; j < n; ++j )
		{
			std::vector<int>::const_iterator i;
			double R_j = 0, delta_j = 0;
			int s = x[j] ? 1 : -1,
			    t = x[j] ? 1 : 0;

			delta_j = s * _c[j];

			for ( i = _T[j].begin(); i != _T[j].end(); ++i )
			{
				if ( v[*i] == t )
				{
					delta_j -= s * _g[*i];
				}
			}

			R_j = delta_j / _c[j];

			if ( R_max < R_j )
			{
				j_max = j;
				R_max = R_j;
			}
		}

		if ( R_max > epsilon )
		{
			_flip( j_max, x, v );
		}
	}

	_x.clear();

	for ( int j = 0; j < n; ++j )
	{
		if ( x[j] )
			_x.insert( j );
	}
	return true;
}

inline double scp_descent::z() const
{
	return _z;
}

inline std::set<int> scp_descent::x() const
{
	return _x;
}

inline int scp_descent::num_iterations() const
{
	return _num_iterations;
}

inline void scp_descent::_flip( int j, std::vector<bool> & x, std::vector<int> & v )
{
	std::vector<int>::const_iterator i;
	int s = x[j] ? 1 : -1;

	x[j] = !x[j];
	_z -= s * _c[j];

	for ( i = _T[j].begin(); i != _T[j].end(); ++i )
	{
		v[*i] -= s;
	}

	++_num_iterations;
}

#endif
