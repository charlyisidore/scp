TARGET = scp

CC      = g++
CFLAGS  = -Iinclude -ansi -Wall -pedantic
LDFLAGS = -lz

RFLAGS   = -O2
DBGFLAGS = -g

SRC    = $(wildcard src/*.cpp)
ROBJ   = $(SRC:.cpp=.r.o)
DBGOBJ = $(SRC:.cpp=.dbg.o)
DEP    = $(SRC:.cpp=.d)

.PHONY: release debug all clean distclean dist

release: $(TARGET)

debug: $(TARGET)-dbg

all: release debug

$(TARGET): $(ROBJ)
	$(CC) -o $@ $^ $(RFLAGS) $(LDFLAGS)

$(TARGET)-dbg: $(DBGOBJ)
	$(CC) -o $@ $^ $(DBGFLAGS) $(LDFLAGS)

src/%.r.o: src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS) $(DFLAGS) $(RFLAGS)

src/%.dbg.o: src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS) $(DFLAGS) $(DBGFLAGS)

src/%.d: src/%.cpp
	@ $(CC) -o $@ -MM -MT '$(<:.cpp=.r.o)' -MT '$(<:.cpp=.dbg.o)' $< $(CFLAGS) $(DFLAGS)

clean:
	@ rm -vf $(TARGET) $(TARGET)-dbg $(ROBJ) $(DBGOBJ) $(DEP)

distclean: clean
	@ find . -name "*~" -exec rm -f {} + -printf "removed '%P'\n"

dist: distclean
	@ tar -vczf "$(TARGET)_$(shell date --rfc-3339='date').tar.gz" \
		--ignore-failed-read \
		--exclude '$(TARGET)_*.tar.gz' \
		--exclude 'instances/*.txt' \
		--exclude 'instances/*.gz' \
		$(shell find . -type f -printf "'%P' ")

-include $(DEP)
