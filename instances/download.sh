#!/bin/bash

# Download all the SCP instances of the OR-library
# See http://people.brunel.ac.uk/~mastjjb/jeb/orlib/scpinfo.html

command -v lynx > /dev/null || { echo "Error: lynx is required to run the script"; exit 1; }

# OR-library instances
lynx -dump http://people.brunel.ac.uk/~mastjjb/jeb/orlib/files/ | grep -oe 'http://people.brunel.ac.uk/~mastjjb/jeb/orlib/files/\(scp.*\.txt\|rail.*\.gz\)' | wget -i - -nv

# Balas aa* and bus* instances
lynx -dump http://www.co.cm.is.nagoya-u.ac.jp/~yagiura/scp/balas/ | grep -oe 'http://www.co.cm.is.nagoya-u.ac.jp/~yagiura/scp/balas/\(.*\.inp\.gz\)' | wget -i - -nv

# Steiner triple covering problem (STCP) instances
lynx -dump http://www.co.cm.is.nagoya-u.ac.jp/~yagiura/scp/stcp/ | grep -oe 'http://www.co.cm.is.nagoya-u.ac.jp/~yagiura/scp/stcp/\(data\..*\.gz\)' | wget -i - -nv

# Wedelin instances
lynx -dump http://www.co.cm.is.nagoya-u.ac.jp/~yagiura/scp/wedelin/ | grep -oe 'http://www.co.cm.is.nagoya-u.ac.jp/~yagiura/scp/wedelin/\(.*\.dat\.gz\)' | wget -i - -nv

# STCP to SCP converter
from_stcp_instance()
{
	read line
	data=($(echo "$line" | tr -d "\r\n"))
	n=${data[0]}
	m=${data[1]}
	c=($(for j in $(seq 1 $n)
	     do echo 1
	     done))
	echo "$m $n"
	echo "${c[@]}"
	for i in $(seq 1 $m)
	do
		read line
		echo "3 $line"
	done
}

# STCP instances from Resende website
#stcptar=steiner-triple-covering.tar.gz
#wget -nv http://mauricio.resende.info/data/steiner-triple-covering.tar.gz -O $stcptar
#for f in $(tar -ztf $stcptar --wildcards --no-anchored 'data.*')
#do
#	tar -xzf $stcptar "$f"
#	bn=$(basename "$f")
#	i=${bn:5}
#
#	# Keep the instance as is (in "stcp" format)
#	echo "Extract instance stcp$i..."
#	mv "$f" "stcp$i.txt"
#
#	# Or convert to "scp" format
#	#echo "Converting stcp$i..."
#	#from_stcp_instance < "$f" > "stcp$i.txt"
#done
#rm -rf $(tar -ztf $stcptar)
#rm -f $stcptar

